<?php

namespace App\Http\Controllers\Admin;

use App\Mail\AdminInformations;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    //

    public function index()
    {

        return view('admin.index')->withAdmins(User::isAdmin()->notMe()->paginate(12));
    }

    public function deleted()
    {
        return view('admin.banned')->withAdmins(User::isAdmin()->notMe()->onlyTrashed()->paginate(12));
    }

    public function create()
    {
        return view('admin.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:100',
            'email' => 'required|email|unique:users',
        ]);


        $password = uniqid(true);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'type' => $request->admin,
            'password' => Hash::make($password)
        ]);

        Mail::to($user)->send(new AdminInformations($request->name, $request->email, $password));

        $request->session()->flash('status', 'Administrateur ajouté !!! le mot de passe lui a été envoyé par mail!');

        return redirect()->back();
    }

    public function delete(User $user, Request $request)
    {
        $user->delete();

        $request->session()->flash('status', 'Administrateur supprimer!');

        return redirect()->back();
    }

    public function restore(User $user, Request $request)
    {
        $user->restore();

        $request->session()->flash('status', 'Operaction effectué!');

        return redirect()->back();
    }
}
