<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminInformations extends Mailable
{
    use Queueable, SerializesModels;
    private $information;
    private $password;
    private $name;
    private $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $password)
    {
        //
        $this->password = $password;
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'formation@azcorporation.net', 'name' => config('app.name')])
            ->markdown('mail.admin')
            ->with([
                'name' => $this->name,
                'password' => $this->password,
                'email' => $this->email,
            ]);
    }
}
