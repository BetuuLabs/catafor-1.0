@extends('layouts.app')

@section('content')
   <div class="sidebar" id="navbarSupportedContent">
      <div class="app-name">
        <img src="images/avatar/5.jpg" alt="" class="avatar">
        <h1 class="h5">{{ Auth::user()->name }}</h1>
        <h1 class="h5"><small>Helo My dearn</small></h1>
      </div>
       <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="#">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Formations</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Mes cours</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Mon compte</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Utilisateurs</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Administrateurs</a>
        </li>
        
      </ul>
   </div>
   <div class="page-content">
      <div class="jumbotron text-center jumbotron-fluid">
        <p class="lead">Votre plateforme d'etude en ligne.</p>
        <h1 class="display-4">{{ config('app.name') }}</h1>
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <div class="row">
          <div class="col-md-8 offset-md-2 p-4">
            <div class="input-group mb-3 input-group-lg">
              <input type="search" class="form-control " placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn">Chercher</button>
              </div>
            </div>
          </div>
        </div>
        <a class="btn btn-primary btn-lg" href="#">Découvrir le catalogue</a>
        <a class="btn btn-outline-primary btn-lg" href="#">En savoir plus</a>
      </div>

      <div class="row mt-2">
        <div class="col-md-12 text-center mb-5">
          <h5 class="text-mutted h6">LEARN NEW SKILLS</h5>
          <h1>Search Genius Courses.</h1>
        </div>
        @for($i = 0; $i < 8; $i++)
          <div class="col-md-4 col-xs-4">
            <div class="card mb-3">
              <span class="card-header text-info">Gratuit</span>
              <img class="card-img-top" src="images/blog/1.png" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title"><a href="#">Card title</a></h5>
                <p class="card-text">
                  We take our mission of increasing global access to quality education seriously.
                </p>
                <a href="#" class="card-link">Card link</a>
                <a href="#" class="card-link">Another link</a>
              </div>
            </div>
          </div>
        @endfor
      </div>
   </div>
@endsection
