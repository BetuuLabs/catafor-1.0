@extends('layouts.dash')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Administrateur</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">En fonction</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
           <div class="col-sm-12">
               @if (session('status'))
                   <div class="alert alert-success" role="alert">
                       {{ session('status') }}
                   </div>
               @endif
               @if($admins->isEmpty())
                   <div class="alert  alert-info alert-dismissible fade show" role="alert" >
                        Vous êtes le seul administrateur, vous pouvez rajouter un autre en cliquant sur ce <a
                           href="{{ route('admin.add') }}">lien</a> .
                       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                       </button>
                   </div>
               @else

                   @foreach($admins as $admin)
                       <div class="col-md-4">
                           <div class="card">
                               <div class="card-body">
                                   <div class="mx-auto d-block">
                                       <img class="rounded-circle mx-auto d-block" src="{{ asset('images/admin.jpg') }}" alt="Card image cap">
                                       <h5 class="text-sm-center mt-2 mb-1">{{ $admin->name }}</h5>
                                       <div class="location text-sm-center"><i class="fa fa-map-marker"></i> {{ $admin->location }}</div>
                                   </div>
                                   <hr>
                                   <div class="card-text text-sm-center">
                                       <a href="#"><i class="fa fa-facebook pr-1"></i></a>
                                       <a href="#"><i class="fa fa-twitter pr-1"></i></a>
                                       <a href="#"><i class="fa fa-linkedin pr-1"></i></a>
                                       <a href="#"><i class="fa fa-pinterest pr-1"></i></a>
                                   </div>
                               </div>
                               <div class="card-footer text-right">
                                   <a href="{{ route('admin.delete',['user' => $admin->id]) }}" class="card-link text-danger" onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">Bannir</a>
                               </div>
                               <form id="delete-form" action="{{ route('admin.delete',['user' => $admin->id]) }}" method="POST" style="display: none;">
                                   @csrf
                                   @method('delete')
                               </form>
                           </div>
                       </div>
                   @endforeach
               @endif
           </div>
    </div>
@endsection
