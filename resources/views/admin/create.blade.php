@extends('layouts.dash')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Administrateurs</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Ajouter administrateur</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
           <div class="col-sm-12">
               <div class="alert  alert-success alert-dismissible fade show" role="alert" style="visibility: hidden">
                   <span class="badge badge-pill badge-success">Success</span> You successfully read this important alert message.
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                   </button>
               </div>
               <div class="card">
                   <div class="card-header">
                       <strong>Administrateur</strong> <small> Ajout d'un administrateur</small>
                   </div>
                   <div class="card-body card-block">
                       @if (session('status'))
                           <div class="alert alert-success" role="alert">
                               {{ session('status') }}
                           </div>
                       @endif
                       <form action="{{ route('admin.store') }}" method="post">
                           @csrf
                           <div class="form-group">
                               <label class=" form-control-label">Nom</label>
                               <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-info-circle"></i></div>
                                   <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Nom">
                                   @if ($errors->has('name'))
                                       <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                   @endif
                               </div>
                               <small class="form-text text-muted">ex. John Doe</small>
                           </div>
                           <div class="form-group">
                               <label class=" form-control-label">Email</label>
                               <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                                   <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                                   @if ($errors->has('email'))
                                       <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                   @endif
                               </div>
                               <small class="form-text text-muted">ex. mail@exemple.com</small>
                           </div>
                           <div class="form-group">
                               <input type="submit" value="Enregistrer" class="btn btn-success">
                       </form>
                   </div>
               </div>
           </div>
    </div>
@endsection
