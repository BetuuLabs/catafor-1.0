@component('mail::message')
    # Vous êtes maintenant administrateur

    Votre compte administrateur a été crée!

        | Nom d'utilisateur       | Email         |Mot de passe  |
        | ------------- |:-------------:| --------:|
        | {{$name}}      | {{$email}}     | {{$password}}      |

    [Acceder à la page d'administration!]: {{route('admin.index')}}
    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
