@component('mail::message')
    # Utilisateur banis

        Cet utilisateur ({{$name}}) a été banis

    [Vérifiez par vous même!]: {{route('admin.deleted')}}
    Merci,<br>
    {{ config('app.name') }}
@endcomponent
