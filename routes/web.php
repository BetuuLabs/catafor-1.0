<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*Route::group(['middleware' => 'auth'], function (){

    //admin module
   Route::group(['prefix' => 'admin'], function (){
        Route::get('/','Admin\AdminController@index')->name('admin.index');
        Route::get('/create','Admin\AdminController@create')->name('admin.add');
        Route::post('/create','Admin\AdminController@store')->name('admin.store');
        Route::delete('/delete/{user}','Admin\AdminController@delete')->name('admin.delete');
        Route::get('/deleted','Admin\AdminController@deleted')->name('admin.deleted');
        Route::put('/restore/{user}','Admin\AdminController@restore')->name('admin.restore');
   });
});
*/